(require 'package)
(package-initialize)

(add-to-list 'package-archives
	     '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives
	     '("marmalade" . "https://marmalade-repo.org/packages/"))

;; Fix HTTP1/1.1 problems
(setq url-http-attempt-keepalives nil)

(defvar packages-to-install '(better-defaults paredit idle-highlight-mode smex
				      helm expand-region company
				      git-gutter company-tern tern
				      editorconfig markdown-mode
				      omnisharp))

(package-refresh-contents)

(dolist (p packages-to-install)
  (when (not (package-installed-p p))
    (package-install p)))

(package-initialize)
;; cc-mode bug: http://debbugs.gnu.org/cgi/bugreport.cgi?bug=18845
(require 'cl)

;; OmniSharp
(setq omnisharp-server-executable-path
      "~/omnisharp-server/OmniSharp/bin/Debug/OmniSharp.exe")
(eval-after-load 'company
  '(add-to-list 'company-backends 'company-omnisharp))
(add-hook 'csharp-mode-hook 'omnisharp-mode)

(require 'smex)
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
;(global-set-key (kbd "M-x") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

(require 'better-defaults)
(ido-mode)
